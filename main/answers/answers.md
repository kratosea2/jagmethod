#JAG Method software developer assessment
## Answers

### 1. SEO (5min)

1) Add site title

2) Add a more descriptive description

3) Add keywords that will describe the website key content

4) Place phrases in a strategic way through the page

### 2. Responsive (15m)

1) Add media query to Newcustom .css

2) Set width for where mobile should be detected

3) Change header style to add left padding

4) Change nav style to centre menu


### 3. Validation (15m)
1) Add Data notation attribute [Required] for all the field that are required

2) Add DataType attribute when you require value validation  e.g [DataType(DataType.EmailAddress)]

3) Add attribute of the value specified by DataType e.g [EmailAddress]

### 4. JavaScript (20m)
1) Added an onsubmit event to the form to increment number of queries processed

2) I save the latest number to a localStorage item and get it onload

### 5. Ajax calls (30m)
1) I prevented the default action of forms which is to submit right away after the submission button is clicked

2) Added ajax call that include type of action this ajax will do (Post), 

3) Set the beforeSend method for before the call is made what should be done

4) Set Complete method for when the call is complete

5) Success method on what should happen when the request was a success

6) SweetAlert popup when for resulting message

7) Added JQueryBlockUI to block screen while processing user request

### 6. Call a REST webservice (25m)
Add any special implemetation instructions here.

Make sure that the WebHost calls the ServiceHost via REST.

### 7. ADO.Net (40m)
No Sql Changes (but if i had to make some I would have moved the columns to the other table from dbo.Lead)

### 8. Poll DB (15m)
No Sql changes, I just added threading time and called function to check the db for new entries every minute

### 9. SignalR (40m)
Add any SQL schema changes here (I didn't have time to attempt this one for you to review)

### 10. Data Analysis (30m)

1) Total Profit
**Answer**
  67870.59685063502800
**SQL**
`select SUM([Earnings]) - SUM([Cost]) from [JAG2019].[dbo].[LeadDetail]`

2) Total Profit (Earnings less VAT)
**Answer**
 -57690.0073230397738

**SQL**
`select (SUM([Earnings]) - SUM([Cost])) - ((SUM([Earnings]) - SUM([Cost])) * 0.15)  from [JAG2019].[dbo].[LeadDetail]`

3) Profitable campaigns
**Answer**
92
161
15
89
3
167
7
141
13
185
16
182
117
77
111
14
137
8

**SQL**
`SELECT distinct [CampaignId]
  FROM [JAG2019].[dbo].[LeadDetail] where (Earnings - Cost) > 0`

4) Average conversion rate
**Answer**
12.979333

**SQL**
`SELECT sum(Earnings) / sum(cost)
  FROM [JAG2019].[dbo].[LeadDetail] where IsSold = 1 and IsAccepted = 1`

5) Pick 2 clients based on Profit & Conversion rate & Why?
**Answer**
205 and 168 Because theres a lot of positve profits for these two
**SQL**
`SELECT distinct [ClientId], (Earnings - cost) as Profit, Earnings / cost as ConversionRate
  FROM [JAG2019].[dbo].[LeadDetail] where IsSold = 1 and IsAccepted = 1`
