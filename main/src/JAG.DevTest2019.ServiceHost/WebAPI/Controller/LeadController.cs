﻿using JAG.DevTest2019.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;

namespace JAG.DevTest2019.LeadService.Controllers
{
    public class LeadController : ApiController
    {
        [HttpPost]
        [ResponseType (typeof(LeadResponse))]
        public HttpResponseMessage Post(Lead request)
        {
            LeadResponse response = new LeadResponse()
            {
                LeadId = 10000000 + new Random().Next(),
                IsCapped = false,
                Messages = new[] { "Success from WebAPI" },
                IsSuccessful = true,
                IsDuplicate = false
            };

            //TODO: 7. Write the lead to the DB
            var connectionString = ConfigurationManager.ConnectionStrings["JAG2019"].ConnectionString;
            var connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                var leadId = response.LeadId;
                var leadInsert = $"insert into dbo.Lead (LeadId, TrackingCode, FirstName, LastName, ContactNumber, Email, ReceivedDateTime, IPAddress, UserAgent, ReferrerURL, IsDuplicate, IsCapped, IsSuccessful)" + 
                    $"values ({leadId},'{request.TrackingCode}','{request.FirstName}','{request.Surname}', '{request.ContactNumber}',"
                     + $"'{request.EmailAddress}', '{DateTime.UtcNow}', '{request.ClientIpAddress}', '{request.UserAgent}', '{request.ReferrerUrl}'," 
                     + $"'{Convert.ToByte(response.IsDuplicate)}', '{Convert.ToByte(response.IsCapped)}', '{Convert.ToByte(response.IsSuccessful)}')";

                var command = new SqlCommand(leadInsert, connection);
                command.ExecuteNonQuery();
                connection.Close();

                var LeadParameterId = 10000000 + new Random().Next();
                var insertLeadParams = $"insert into dbo.LeadParameter values ({LeadParameterId},{leadId},'{""}', '{""}')";
                connection.Open();
                command = new SqlCommand(insertLeadParams, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            Console.WriteLine($"Lead received {request.FirstName} {request.Surname}");

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
