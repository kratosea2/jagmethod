﻿using JAG.DevTest2019.ServiceHost.WebAPI;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace JAG.DevTest2019.ServiceHost
{
    class Program
    {
        private static IDisposable _WebApiServiceHost;

        static void Main(string[] args)
        {
            Console.WriteLine($"Starting WebAPI");
            int apiPort = 8099;
            string apiHost = "http://localhost";
            string url = $"{apiHost}:{apiPort}";

            _WebApiServiceHost = WebApp.Start<WebApiStartup>(url);
            Console.WriteLine($"WebAPI hosted on {url}");

            var processStartTime = TimeSpan.Zero;
            var processEndTime = TimeSpan.FromMinutes(1);
            var date = DateTime.UtcNow.ToString();
            var timer = new System.Threading.Timer((e) =>
            {
                Console.WriteLine(GetNumberOfNewLeads(date));
            }, null, processStartTime, processEndTime);

            Console.WriteLine($"Press enter to exit");
            Console.ReadLine();
        }

        public static string GetNumberOfNewLeads(string date)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["JAG2019"].ConnectionString;
            var connection = new SqlConnection(connectionString);
            var queryString = $"select count(*) from dbo.Lead where ReceivedDateTime > '{date}'";
            var records = Convert.ToInt32(ConfigurationManager.AppSettings["leads"]);
            Configuration configuration = ConfigurationManager. OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

            try
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();

                var numberOfRecords = (int)command.ExecuteScalar();

                return $"{numberOfRecords} Were added in the last minute";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.InnerException.Message);
            }
        }
    }
}
