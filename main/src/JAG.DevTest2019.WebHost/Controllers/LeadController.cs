﻿using JAG.DevTest2019.Host.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace JAG.DevTest2019.Host.Controllers
{
    public class LeadController : Controller
    {
        public ActionResult Index()
        {
            return View(new LeadViewModel());
        }

        public ActionResult SubmitLead(LeadViewModel model)
        {
            //TODO: 6. Call the WebAPI service here & pass results to UI
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:8099/api/Lead/");

                var hostPost = client.PostAsJsonAsync<LeadViewModel>("request", model);
                hostPost.Wait();

                var response = hostPost.Result;

                if (response.IsSuccessStatusCode)
                {
                    LeadViewModel result = new LeadViewModel()
                    {
                        Results = new LeadResultViewModel()
                        {
                            LeadId = new Random().Next(),
                            IsSuccessful = true,
                            Message = $"Thank you for submitting your details.\n Your reference number is: {""}"
                        }
                    };

                    return Json(result.Results);
                }
                else
                {

                    LeadViewModel result = new LeadViewModel()
                    {
                        Results = new LeadResultViewModel()
                        {
                            LeadId = new Random().Next(),
                            IsSuccessful = false,
                            Message = "You details were not successfully submitted please contact us"
                        }
                    };
                }

                return null;
            }
        }
    }
}